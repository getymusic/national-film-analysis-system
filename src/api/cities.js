import request from '../utils/request'
export function getCities(){
    return request({
        url:'https://elm.cangdu.org/v1/cities?type=guess',
        method:'get'
    })
}