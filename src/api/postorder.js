import request from '@/utils/request'
export function postOrder(orderNum,userid,movieName,orderCinema,seat,time,price,movieType) {
  return request({
    url: '/Oinsert',
    method: 'post',
    params: {
      订单号: orderNum,
      用户ID: userid,
      电影名: movieName,
      影院: orderCinema,
      座位: seat,
      时间: time,
      价格: price,
      类别: movieType,
    }
  })
}

