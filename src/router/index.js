import Vue from 'vue'
import VueRouter from 'vue-router'
import home from '../components/home.vue'
import users from '../components/users.vue'
import cinema from '../components/cinema.vue'
import NotFound from '../components/notFound.vue'
import About from '../components/About.vue'
import movie from '../components/movie'
import cin1 from '../components/cin1'
import cin3 from '../components/cin3'
import cin5 from '../components/cin5'
import cin7 from '../components/cin7'
import rest from '../components/rest'
import pre1 from '../components/pre1'
import pre2 from '../components/pre2'
import pre3 from '../components/pre3'
import pre4 from '../components/pre4'
import detail from '../components/detail'
import detail2 from '../components/detail2'
import detail3 from '../components/detail3'
import detail4 from '../components/detail4'
import detail5 from '../components/detail5'
import detail6 from '../components/detail6'
import myTDC from '../components/myTDC'
import myFile from '../components/myFile'
import help from '../components/help'
import setting from '../components/setting'
import login from '../components/login.vue'
import orders from '../components/orders'
import orderdetail from '../components/orderdetail'

Vue.use(VueRouter)

export default new VueRouter({
  mode: 'history', // 去掉路由地址的#
  routes: [
    //    ‘/’为默认加载页面
    {
      path: '/',
      component: login
    },
    {
      path: '/home',
      component: home
    },
    {
      path: '/about',
      component: About
    },
    {
      path: '/cinema',
      component: cinema
    },
    {
      path: '/pre1',
      component: pre1
    },
    {
      path: '/pre2',
      component: pre2
    },
    {
      path: '/pre3',
      component: pre3
    },
    {
      path: '/pre4',
      component: pre4
    },
    {
      path: '/detail',
      component: detail
    },
    {
      path: '/detail2',
      component: detail2
    },
    {
      path: '/detail3',
      component: detail3
    },
    {
      path: '/detail4',
      component: detail4
    },
    {
      path: '/detail5',
      component: detail5
    },
    {
      path: '/detail6',
      component: detail6
    },
    {
      path: '/users',
      component: users
    },
    {
      path: '/movie',
      component: movie
    },
    {
      path: '/cin1',
      component: cin1
    },
    {
      path: '/cin3',
      component: cin3
    },
    {
      path: '/cin5',
      component: cin5
    },
    {
      path: '/cin7',
      component: cin7
    },
    {
      path: '/rest',
      component: rest
    },
    {
      path: '/myTDC',
      component: myTDC
    },
    {
      path: '/myFile',
      component: myFile
    },
    {
      path: '/help',
      component: help
    },
    {
      path: '/setting',
      component: setting
    },
    {
      path: '/orders',
      component: orders
    },
    {
      path: '/orderdetail',
      component: orderdetail
    },
    //错误路由 重新定向(404)
    {
      path: '**',
      component: NotFound
    }
  ]
})
