import Vue from "vue"
import Vuex from "vuex"

Vue.use(Vuex)

export default new Vuex.Store({
  // state 为全局共享的数据变量对象
  state: {
    name: "张三"
  },
  //当数据有变更时所调用的变更方法
  mutations: {
    changeName (state, changename) {
      state.name = changename
    }
  }
})
