import axios from 'axios'
import store from '../store'
import router from '../router'

// 创建axios实例
const service = axios.create({
  baseURL: 'http://localhost:12345',
  timeout: 15000 // 请求超时时间
})

// request拦截器
service.interceptors.request.use(config => {
  return config
}, error => {
  // Do something with request error
  console.log(error) // for debug
  Promise.reject(error)
})

// respone拦截器
service.interceptors.response.use(
  response => {
    /**
     * code为1000000是没登录 可结合自己业务进行修改
     */
    let res = response.data
    // if (res.code && res.code === '1000000') {
    //   localStorage.permission = ''
    //   this.$router.push('/login')
    // }
    return res
  },
  error => {
    console.log('err' + error) // for debug
  }
)


export default service
