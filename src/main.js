// The Vue build version to load with the `import` command
// (runtime-only or standalone) has been set in webpack.base.conf with an alias.
import Vue from 'vue'
import App from './App'
import router from './router'
import MuseUI from 'muse-ui'
import VeeValidate from 'vee-validate'
import axios from 'vue-axios'
import VueAxios from 'axios'
import VueJsonp from 'vue-jsonp'
import store from './store'
import 'weixin-js-sdk'
import 'muse-ui/dist/muse-ui.css'
import 'jquery.cookie'
import ViewUI from 'view-design'
import 'view-design/dist/styles/iview.css'
import {getCities} from './api/cities'
import { Search } from 'vant';
import { ContactCard, ContactList, ContactEdit } from 'vant';
import echarts from 'echarts';
import ElementUI from 'element-ui'
import VueLazyload from 'vue-lazyload'
import VueCookies from 'vue-cookies'



Vue.use(VueLazyload, {
    loading: require('../assets/images/nine.png'),//加载中图片，一定要有，不然会一直重复加载占位图
    error: require('../assets/images/nine.png')  //加载失败图片
    });
Vue.prototype.$http = axios
Vue.use(MuseUI)
Vue.use(ViewUI)
Vue.use(VueAxios,axios)
Vue.use(VueJsonp)
Vue.use(ElementUI)
Vue.use(VeeValidate)
Vue.use(getCities);
Vue.use(Search);
Vue.use(echarts);
Vue.use(ContactCard);
Vue.use(ContactList);
Vue.use(ContactEdit);
Vue.use(require('vue-cookies'))
Vue.use(VueCookies)


Vue.config.productionTip = false
new Vue({
  el: '#app',
  store,
  router,
  data() {
    return {
      data: {
        //用户手机号
        userPhion: '',
        //用户验证码
        userCode: ''
      }
    }
  },
  render: h => h(App),
  components: {
    App
  },
  template: '<App/>',

  //  methods是绑定方法到Vue实例对象上，供当前Vue组件作用域内通过上下文this使用，未调用不会执行。
  methods: {},
  // mounted:在模板渲染成html后调用，通常是初始化页面完成后，再对html的dom节点进行一些需要的操作。
  mounted: function () {}
})

$cookies.config()
