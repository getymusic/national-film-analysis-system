# vue-weixin

> A Vue.js project

## Build Setup

``` bash
# install dependencies
npm install

# serve with hot reload at localhost:8080
npm run dev

# build for production with minification
npm run build

# build for production and view the bundle analyzer report
npm run build --report
```
目录：
personal：信息展示页
generalRegistration：普通报名页面
IntelligentRegistration：智能报名页面
newsAndInformation：新闻资讯页面
footer：页面底部
header：页面头部


