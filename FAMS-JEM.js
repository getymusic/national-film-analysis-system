﻿var http=require('http'); 
var urlLib=require('url');
var mysql=require('mysql');
var express=require('express');
var bodyParser=require('body-parser');
var MongoClient = require('mongoose');
var app=express();//实例化一个模块
app.use(bodyParser.urlencoded({extend:true}));
var router = express.Router();
var sql;
var sqlParam;

// //定义一个mysql链接
// try {
//     var connection=mysql.createConnection({
//         host:'127.0.0.1',
//         port:'16608',
//         user:'root',
//         password:'',
//         database:'dbypssj'
//     });    
// } catch (error) {
//   console.log(error.toString());  
// }

//定义一个mongodb链接
MongoClient.connect('mongodb://localhost:27017/db0',{ useNewUrlParser: true },function (err,db) {
    console.log('test')
});


//---------------------------------------------------------
var Schema1 = MongoClient.Schema; //创建一个Schema模型骨架，并且设置好user模版
var userSchema = new Schema1({
    用户名:{type:String},
    用户ID:{type:String},
    VIP等级:{type:String},
    蓝宝石:{type:String},
    积分数:{type:String},
    动作:{type:String},
    科幻:{type:String},
    奇幻:{type:String},
    动画:{type:String},
    冒险:{type:String},
    剧情:{type:String},
    喜剧:{type:String},
    悬疑:{type:String},
    爱情:{type:String},
    犯罪:{type:String},
    HeaderImg:{type:String},
});
var UserInfo = MongoClient.model("个人中心",userSchema);  //Schema发布一个User的Model
app.post('/insert',function (req,res) {  //处理insert的请求
    
    res.setHeader("Access-Control-Allow-Origin", "*"); 
    console.log('请求成功')
    console.log(req.query.用户ID)
    var user_1 = new UserInfo({
        用户名:req.query.用户名,
        用户ID:req.query.用户ID,
        VIP等级:req.query.VIP等级,
        蓝宝石:req.query.蓝宝石,
        积分数:req.query.积分数,
        动作:req.query.动作,
        科幻:req.query.科幻,
        奇幻:req.query.奇幻,
        动画:req.query.动画,
        冒险:req.query.冒险,
        剧情:req.query.剧情,
        喜剧:req.query.喜剧,
        悬疑:req.query.悬疑,
        爱情:req.query.爱情,
        犯罪:req.query.犯罪,
        HeaderImg:req.query.HeaderImg,
    });
    //存储数据
    user_1.save(function (err) {
        if(err){
            res.end('Error');
            return next();
        }
        res.end()
    })
});
app.get('/select',function (req,res) {  //处理select的get请求
    console.log('请求成功');
    console.log(req.query.用户ID)
res.setHeader("Access-Control-Allow-Origin", "*"); 
    var user_1 = new UserInfo({
        用户名:req.query.用户名,
        用户ID:req.query.用户ID,
        VIP等级:req.query.VIP等级,
        蓝宝石:req.query.蓝宝石,
        积分数:req.query.积分数,
        动作:req.query.动作,
        科幻:req.query.科幻,
        奇幻:req.query.奇幻,
        动画:req.query.动画,
        冒险:req.query.冒险,
        剧情:req.query.剧情,
        喜剧:req.query.喜剧,
        悬疑:req.query.悬疑,
        爱情:req.query.爱情,
        犯罪:req.query.犯罪,
        HeaderImg:req.query.HeaderImg,
    });
    //查询数据    find({})意为返回所有文档
    UserInfo.find(function (err, results) {
        if(err){
            res.end('Error');
            return next();
        }
        res.json(results);//以json数据类型返回值 返回到进行请求的页面 results是数据库存储的所有数据

    });
});

app.delete('/delete',function (req,res) {  //处理delect的get请求
        console.log('删除');
        //删除数据
res.setHeader("Access-Control-Allow-Origin", "*"); 
    var user_1 = new UserInfo({
            用户名:{type:String},
            用户ID:{type:String},
            VIP等级:{type:String},
            蓝宝石:{type:String},
            积分数:{type:String},
            动作:{type:String},
            科幻:{type:String},
            奇幻:{type:String},
            动画:{type:String},
            冒险:{type:String},
            剧情:{type:String},
            喜剧:{type:String},
            悬疑:{type:String},
            爱情:{type:String},
            犯罪:{type:String},
            HeaderImg:{type:String},
        });
        let whereStr = {"用户ID":user_1.用户ID};
        User.deleteMany(whereStr,function (err, res) {
            if(err){
                res.end('Error');
                return
            }else{
                console.log('刪除成功');
            }
        });
        res.end();
    });    

app.put('/update',function (req,res) {
        console.log("x");
res.setHeader("Access-Control-Allow-Origin", "*"); 
    var user_1 = new UserInfo({
            用户名:{type:String},
            用户ID:{type:String},
            VIP等级:{type:String},
            蓝宝石:{type:String},
            积分数:{type:String},
            动作:{type:String},
            科幻:{type:String},
            奇幻:{type:String},
            动画:{type:String},
            冒险:{type:String},
            剧情:{type:String},
            喜剧:{type:String},
            悬疑:{type:String},
            爱情:{type:String},
            犯罪:{type:String},
            HeaderImg:{type:String},
        });
        let whereStr = {"用户ID":user_1.用户ID};
        let updateStr = {$set:{"用户名":user_1.用户名,"VIP等级":user_1.VIP等级,"蓝宝石":user_1.蓝宝石,
    "积分数":user_1.积分数,"动作":user_1.动作}};
        User.updateOne(whereStr,updateStr,function (err,res) {
            if (err) {
                console.log('加入失败：' + err);
                return
            } else {
                // add
                console.log("修改成功")
            }
            
        });
        res.end();
    });
    // ------------------------------------------------------------------
    

    var Schema2 = MongoClient.Schema; //创建一个Schema模型骨架，并且设置好user模版
    var orderSchema = new Schema2({
    订单号:{type:String},
    用户ID:{type:String},
    电影名:{type:String},
    影院:{type:String},
    座位:{type:String},
    时间:{type:String},
    价格:{type:String},
    类别:{type:String}
});
var OrderInfo = MongoClient.model("订单",orderSchema);  //Schema发布一个Order的Model
app.post('/Oinsert',function (req,res) {  //处理insert的请求
    console.log('请求成功')
    console.log(req.query.订单号)
res.setHeader("Access-Control-Allow-Origin", "*");  

    var order_1 = new OrderInfo({
        订单号:req.query.订单号,
        用户ID:req.query.用户ID,
        电影名:req.query.电影名,
        影院:req.query.影院,
        座位:req.query.座位,
        时间:req.query.时间,
        价格:req.query.价格,
        类别:req.query.类别,
    });
    //存储数据
    order_1.save(function (err) {
        if(err){
            res.end('Error');
            return next();
        }
        res.end()
    })
});
app.get('/Oselect',function (req,res) {  //处理select的get请求
    console.log('请求成功');
    console.log(req.query.订单号)
res.setHeader("Access-Control-Allow-Origin", "*"); 
    var user_1 = new UserInfo({
        订单号:req.query.订单号,
        用户ID:req.query.用户ID,
        电影名:req.query.电影名,
        影院:req.query.影院,
        座位:req.query.座位,
        时间:req.query.时间,
        价格:req.query.价格,
        类别:req.query.类别,
    });
    //查询数据    find({})意为返回所有文档
    OrderInfo.find(function (err, results) {
        if(err){
            res.end('Error');
            return next();
        }
        res.json(results);//以json数据类型返回值 返回到进行请求的页面 results是数据库存储的所有数据

    });
});

app.delete('/Odelete',function (req,res) {  //处理delect的get请求
        console.log('删除');
        //删除数据
res.setHeader("Access-Control-Allow-Origin", "*"); 
    var order_1 = new OrderInfo({
            订单号:{type:String},
            用户ID:{type:String},
            电影名:{type:String},
            影院:{type:String},
            座位:{type:String},
            时间:{type:String},
            价格:{type:String},
            类别:{type:String}
        });
        let whereStr = {"订单号":order_1.订单号};
        Order.deleteMany(whereStr,function (err, res) {
            if(err){
                res.end('Error');
                return
            }else{
                console.log('刪除成功');
            }
        });
        res.end();
    });    

app.put('/Oupdate',function (req,res) {
        console.log("x");
res.setHeader("Access-Control-Allow-Origin", "*"); 
    var order_1 = new OrderInfo({
            订单号:{type:String},
            用户ID:{type:String},
            电影名:{type:String},
            影院:{type:String},
            座位:{type:String},
            时间:{type:String},
            价格:{type:String},
            类别:{type:String}
        });
        let whereStr = {"订单号":order_1.订单号};
        let updateStr = {$set:{"订单号":order_1.订单号,"用户ID":order_1.用户ID,
    "电影名":order_1.电影名,"影院":order_1.影院,"座位":order_1.座位,"时间":order_1.时间,
    "价格":order_1.价格,"类别":order_1.类别}};
        Order.updateOne(whereStr,updateStr,function (err,res) {
            if (err) {
                console.log('加入失败：' + err);
                return
            } else {
                // add
                console.log("修改成功")
            }
            
        });
        res.end();
    });



    // ------------------------------------------------------------------

   var Schema3 = MongoClient.Schema; //创建一个Schema模型骨架，并且设置好movie模版
    var movieSchema = new Schema3({
    电影名:{type:String},
    演员1:{type:String},
    演员2:{type:String},
    演员3:{type:String},
    演员4:{type:String},
    上映时间:{type:String},
    状态:{type:String},
    
});
var MovieInfo = MongoClient.model("电影信息",movieSchema);  //Schema发布一个Order的Model
app.post('/Minsert',function (req,res) {  //处理insert的请求
    
    res.setHeader("Access-Control-Allow-Origin", "*"); 
    console.log('请求成功')
    console.log(req.query.电影名)
    var movie_1 = new MovieInfo({
        电影名:req.query.电影名,
        演员1:req.query.演员1,
        演员2:req.query.演员2,
        演员3:req.query.演员3,
        演员4:req.query.演员4,
        上映时间:req.query.上映时间,
        状态:req.query.状态


    });
    //存储数据
   movie_1.save(function (err) {
        if(err){
            res.end('Error');
            return next();
        }
        res.end()
    })
});
app.get('/Mselect',function (req,res) {  //处理select的get请求
    console.log('请求成功');
    console.log(req.query.电影名)
res.setHeader("Access-Control-Allow-Origin", "*"); 
    var movie_1 = new MovieInfo({
        电影名:req.query.电影名,
        演员1:req.query.演员1,
        演员2:req.query.演员2,
        演员3:req.query.演员3,
        演员4:req.query.演员4,
        上映时间:req.query.上映时间,
        状态:req.query.状态
    });
    //查询数据    find({})意为返回所有文档
    MovieInfo.find(function (err, results) {
        if(err){
            res.end('Error');
            return next();
        }
        res.json(results);//以json数据类型返回值 返回到进行请求的页面 results是数据库存储的所有数据

    });
});

app.delete('/Mdelete',function (req,res) {  //处理delect的get请求
        console.log('删除');
        //删除数据
res.setHeader("Access-Control-Allow-Origin", "*"); 
    var movie_1 = new MovierInfo({
        电影名:{type:String},
        演员1:{type:String},
        演员2:{type:String},
        演员3:{type:String},
        演员4:{type:String},
        上映时间:{type:String},
        状态:{type:String},

        });
        let whereStr = {"电影名":movie_1.电影名};
        Movie.deleteMany(whereStr,function (err, res) {
            if(err){
                res.end('Error');
                return
            }else{
                console.log('刪除成功');
            }
        });
        res.end();
    });    

app.put('/Mupdate',function (req,res) {
        console.log("x");
res.setHeader("Access-Control-Allow-Origin", "*"); 
    var movie_1 = new MovieInfo({
        电影名:{type:String},
        演员1:{type:String},
        演员2:{type:String},
        演员3:{type:String},
        演员4:{type:String},
        上映时间:{type:String},
        状态:{type:String},
        });
        let whereStr = {"电影名":movie_1.电影名};
        let updateStr = {$set:{"电影名":movie_1.电影名,"演员1":movie_1.演员1,
    "演员2":movie_1.演员2,"演员3":movie_1.演员3,"演员4":movie_1.演员4,
    "上映时间":movie_1.上映时间,"状态":movie_1.状态,}};
       Movie.updateOne(whereStr,updateStr,function (err,res) {
            if (err) {
                console.log('加入失败：' + err);
                return
            } else {
                // add
                console.log("修改成功")
            }
            
        });
        res.end();
    });

//--------------------------------------------------------------------------------

  var Schema4= MongoClient.Schema; //创建一个Schema模型骨架，并且设置好user模版
    var cinemaSchema = new Schema4({
    影院名:{type:String},
    影院ID:{type:String},
    评分:{type:String},
    地址:{type:String},
    状态:{type:String}
    
});
var CinemaInfo = MongoClient.model("影院信息",cinemaSchema);  //Schema发布一个Order的Model
app.post('/Cinsert',function (req,res) {  //处理insert的请求
    
    res.setHeader("Access-Control-Allow-Origin", "*"); 
    console.log('请求成功')
    console.log(req.query.影院名)
    var cinema_1 = new CinemaInfo({
        影院名:req.query.影院名,
        影院ID:req.query.影院ID,
        评分:req.query.评分,
        地址:req.query.地址,
       状态:req.query.状态,
    });
    //存储数据
   cinema_1.save(function (err) {
        if(err){
            res.end('Error');
            return next();
        }
        res.end()
    })
});
app.get('/Cselect',function (req,res) {  //处理select的get请求
    console.log('请求成功');
    console.log(req.query.影院名)
res.setHeader("Access-Control-Allow-Origin", "*"); 
    var cinema_1 = new CinemaInfo({
       影院名:req.query.影院名,
        影院ID:req.query.影院ID,
        评分:req.query.评分,
        地址:req.query.地址,
       状态:req.query.状态,
    });
    //查询数据    find({})意为返回所有文档
    CinemaInfo.find(function (err, results) {
        if(err){
            res.end('Error');
            return next();
        }
        res.json(results);//以json数据类型返回值 返回到进行请求的页面 results是数据库存储的所有数据

    });
});

app.delete('/Cdelete',function (req,res) {  //处理delect的get请求
        console.log('删除');
        //删除数据
res.setHeader("Access-Control-Allow-Origin", "*"); 
    var cinema_1 = new CinemaInfo({
        影院名:{type:String},
    影院ID:{type:String},
    评分:{type:String},
    地址:{type:String},
    状态:{type:String}
        });
        let whereStr = {"影院名":cinema_1.影院名};
        Cinema.deleteMany(whereStr,function (err, res) {
            if(err){
                res.end('Error');
                return
            }else{
                console.log('刪除成功');
            }
        });
        res.end();
    });    

app.put('/Cupdate',function (req,res) {
        console.log("x");
res.setHeader("Access-Control-Allow-Origin", "*"); 
    var cinema_1 = newCinemaInfo({
    影院名:{type:String},
    影院ID:{type:String},
    评分:{type:String},
    地址:{type:String},
    状态:{type:String}
        });
        let whereStr = {"影院名":cinema_1.电影名};
        let updateStr = {$set:{"影院名":cinema_1.影院名,"影院ID":cinema_1.影院ID,
    "评分":cinema_1.评分,"地址":cinema_1.地址,"状态":cinema_1.状态}};
        cinema.updateOne(whereStr,updateStr,function (err,res) {
            if (err) {
                console.log('加入失败：' + err);
                return
            } else {
                // add
                console.log("修改成功")
            }
            
        });
        res.end();
    });



    var db = MongoClient.connection;
    db.on('open',function () {
        console.log('MongoDB链接成功')
    })
    db.on('error',function () {
        console.log('MongoDB连接失败');
    })
    
    
    app.listen(12345);
    //表示在控制台上提示用户的输入信息
    console.log("server is running in port 12345")